//
//  LerigosConnectCloud.m
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import "LerigosConnectCloud.h"

@interface LerigosConnectCloud ()

@end

@implementation LerigosConnectCloud

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.customerPicture.layer.cornerRadius = self.customerPicture.frame.size.width / 2;
    self.customerPicture.clipsToBounds = true;
    self.customerPicture.userInteractionEnabled = true;

    NSLayoutConstraint *leftConstraint =[NSLayoutConstraint
                                         constraintWithItem:scrollController
                                         attribute:NSLayoutAttributeLeading
                                         relatedBy:0
                                         toItem:self.view
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1.0
                                         constant:0];
    [self.view addConstraint:leftConstraint];
    
    NSLayoutConstraint *rightConstraint =[NSLayoutConstraint
                                          constraintWithItem:scrollController
                                          attribute:NSLayoutAttributeTrailing
                                          relatedBy:0
                                          toItem:self.view
                                          attribute:NSLayoutAttributeRight
                                          multiplier:1.0
                                          constant:0];
    [self.view addConstraint:rightConstraint];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardDidHideNotification object:nil];
    
    [scrollController setScrollEnabled:true];
    [scrollController setContentSize:CGSizeMake(320, 374)];
    
    [self.firstName setDelegate:self];
    [self.lastName setDelegate:self];
    [self.livingCountry setDelegate:self];

    [self CurrentLocationIdentifier];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.firstName resignFirstResponder];
    [self.lastName resignFirstResponder];
    [self.livingCountry resignFirstResponder];
    
    return YES;
}

- (void) keyboardWasShown:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0);
    scrollController.contentInset = contentInsets;
    scrollController.scrollIndicatorInsets = contentInsets;
}

- (void) keyboardWillBeHidden:(NSNotification *)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollController.contentInset = contentInsets;
    scrollController.scrollIndicatorInsets = contentInsets;
}

#pragma mark - Button Controller Event for Layouts

- (IBAction)saveCustomerChanges:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)closeCustomerController:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

#pragma mark - Location Pilingation

-(void)CurrentLocationIdentifier {
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (!(error))
         {
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             NSLog(@"\nCurrent Location Detected\n");
             NSLog(@"placemark %@",placemark);
             NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
             NSString *Address = [[NSString alloc]initWithString:locatedAt];
             NSString *Area = [[NSString alloc]initWithString:placemark.locality];
             NSString *Country = [[NSString alloc]initWithString:placemark.country];
             NSString *CountryArea = [NSString stringWithFormat:@"%@, %@", Area,Country];
             NSLog(@"%@",CountryArea);
         } else {
             NSLog(@"Geocode failed with error %@", error);
             NSLog(@"\nCurrent Location Not Detected\n");
         }
         /*---- For more results
          placemark.region);
          placemark.country);
          placemark.locality);
          placemark.name);
          placemark.ocean);
          placemark.postalCode);
          placemark.subLocality);
          placemark.location);
          ------*/
     }];
}

@end
