//
//  LerigosConnectTimeline.m
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import "LerigosConnectTimeline.h"
#import "LerigosConnectCell.h"
#import "ArtistClubLounge.h"
#import "LerigosConnectLounge.h"

#import "LoginAtConnect.h"

#import "SVProgressHUD.h"
#import <Parse/Parse.h>

@interface LerigosConnectTimeline () {
    NSArray *at;
}

@end

@implementation LerigosConnectTimeline

- (void)viewDidLoad {
    [super viewDidLoad];

    PFUser *currentUser = [PFUser currentUser];
    if (currentUser) {
        // do stuff with the user
    } else {
        LoginAtConnect *loginController = [[LoginAtConnect alloc] initWithNibName:@"LoginAtConnect" bundle:nil];
        [self presentViewController:loginController animated:true completion:nil];
    }
    
    [self loadLerigosConnectMessages];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Load Connect News

- (void)loadLerigosConnectMessages {
    PFQuery *query = [PFQuery queryWithClassName:@"Connect"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *a, NSError *error) {
        if (!error) {
            self.ninjas = [[NSArray alloc] initWithArray:a];
            artistName = [self.ninjas valueForKey:@"artistName"];
            artistMessage = [self.ninjas valueForKey:@"artistMessage"];
            [tableView reloadData];
        }
    }];
}

#pragma mark - Login in Artist or User area

- (IBAction)authWithRole:(id)sender {
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"_User"];
    [query getObjectInBackgroundWithId:currentUser.objectId block:^(PFObject *artistCheck, NSError *error) {
        NSLog(@"%@", artistCheck);
    }];
    
    LerigosConnectLounge *centerTest = [[LerigosConnectLounge alloc] initWithNibName:@"LerigosConnectLounge" bundle:nil];
    [self presentViewController:centerTest animated:true completion:nil];
    
    /*
     ArtistClubLounge *artistLounge = [[ArtistClubLounge alloc] initWithNibName:@"ArtistClubLounge" bundle:nil];
     [self presentViewController:artistLounge animated:true completion:nil];
     NSLog(@"Artist");
     
     LoginAtConnect *userLounge = [[LoginAtConnect alloc] initWithNibName:@"LoginAtConnect" bundle:nil];
     [self presentViewController:userLounge animated:true completion:nil];
     NSLog(@"User");
    */
}

- (IBAction)logOutViewController:(id)sender {
    [PFUser logOutInBackground];
    LoginAtConnect *loginController = [[LoginAtConnect alloc] initWithNibName:@"LoginAtConnect" bundle:nil];
    [self presentViewController:loginController animated:true completion:nil];
}

#pragma mark - Lerigos Music Connect News

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [artistName count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LerigosConnectCell *cellController = [tableView dequeueReusableCellWithIdentifier:@"connectNews"];
    if (!cellController) {
        [tableView registerNib:[UINib nibWithNibName:@"LerigosConnectCell" bundle:nil] forCellReuseIdentifier:@"connectNews"];
        cellController = [tableView dequeueReusableCellWithIdentifier:@"connectNews"];
    }
    
    return cellController;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(LerigosConnectCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
        cell.artistName.text = [artistName objectAtIndex:indexPath.row];
        cell.artistMessage.text = [artistMessage objectAtIndex:indexPath.row];
}

@end
