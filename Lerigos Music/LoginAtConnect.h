//
//  LoginAtConnect.h
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 24.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginAtConnect : UIViewController <UITextFieldDelegate> {
    IBOutlet UIScrollView *scrollFields;
}

@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;

@end
