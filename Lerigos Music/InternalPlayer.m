//
//  InternalPlayer.m
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import "InternalPlayer.h"

@interface InternalPlayer ()

@end

@implementation InternalPlayer

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Multimedia Elements Controller

- (IBAction)playSongWithEvent:(id)sender {
    
}

#pragma mark - Sharing Option Controller

- (IBAction)sharePostWithTrack:(id)sender {
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        mySLComposerSheet = [[SLComposeViewController alloc] init];
        mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [mySLComposerSheet setInitialText:[NSString stringWithFormat:(@"I'm listening the track %@ using Lerigos Music service.", self.artistSong.text), mySLComposerSheet.serviceType]];
        [mySLComposerSheet addImage:self.artistArtwork.image];
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSString *output;
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                output = @"You canceled sending your tweets on Twitter. All changes are canceled.";
                break;
            case SLComposeViewControllerResultDone:
                output = @"Your tweet has been successfully published. You can monitor the activity of your post on Twitter, or from your device. Thank you for using Lerigos Music service.";
                break;
            default:
                break;
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter" message:output delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
    }];

}

@end
