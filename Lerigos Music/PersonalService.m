//
//  PersonalService.m
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import "PersonalService.h"
#import "PersonalClubLounge.h"

@interface PersonalService ()

@end

@implementation PersonalService

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)freeTrialEvaluation:(id)sender {
    PersonalClubLounge *clubService = [[PersonalClubLounge alloc] initWithNibName:@"PersonalClubLounge" bundle:nil];
    [self presentViewController:clubService animated:true completion:nil];
}

- (IBAction)closeCurrentController:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

@end
