//
//  MusicLibrary.h
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 23.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface MusicLibrary : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UITableView *tableView;
}

@property (nonatomic, strong) MPMediaQuery *artistStream;
@property (nonatomic, strong) MPMediaQuery *artistsQuery;
@property (nonatomic, strong) NSArray *artistsArray,*sectionedArtistsArray;
@property (nonatomic, strong) UILocalizedIndexedCollation *collation;

- (NSArray *)partitionObjects:(NSArray *)array collationStringSelector:(SEL)selector;

@end
