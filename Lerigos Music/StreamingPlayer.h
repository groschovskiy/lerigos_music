//
//  StreamingPlayer.h
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVAudioPlayer.h>
#import <AVFoundation/AVPlayerItem.h>

@interface StreamingPlayer : UIViewController {
    SLComposeViewController *mySLComposerSheet;
}

@property (nonatomic, retain) AVAudioPlayer *player;

@property (weak, nonatomic) NSString *protocolStationName;
@property (weak, nonatomic) NSString *protocolStationTrack;
@property (weak, nonatomic) NSString *protocolStationStream;

@property (weak, nonatomic) IBOutlet UILabel *stationName;
@property (weak, nonatomic) IBOutlet UILabel *stationTrack;
@property (weak, nonatomic) IBOutlet UIImageView *stationImage;

@end
