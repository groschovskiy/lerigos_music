//
//  LerigosConnectCenter.h
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 06.08.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LerigosConnectCenter : UIViewController {
    IBOutlet UIScrollView *scrollController;
}

@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIImageView *profilePicture;

@end
