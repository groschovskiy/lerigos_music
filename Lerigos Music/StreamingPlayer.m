//
//  StreamingPlayer.m
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import "StreamingPlayer.h"
#import <MediaPlayer/MediaPlayer.h>

@interface StreamingPlayer ()

@end

@implementation StreamingPlayer

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)viewDidLoad {
    self.stationName.text = self.protocolStationName;
    self.stationTrack.text = self.protocolStationTrack;
    
    MPNowPlayingInfoCenter* mpic = [MPNowPlayingInfoCenter defaultCenter];
    mpic.nowPlayingInfo = @{
                            MPMediaItemPropertyTitle:self.stationTrack.text,
                            MPMediaItemPropertyArtist:self.stationName.text
                            };
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    swipe.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipe];
    
    NSString *urlAddress = self.protocolStationStream;
    NSURL *broadcastingURL = [[NSURL alloc] initWithString:urlAddress];
    self.player = [[AVPlayer alloc] initWithURL:broadcastingURL];
    [self.player play];
    
    [self becomeFirstResponder];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    if (self.player.rate == 1.0) {
        [self.player pause];
    } else {
        [self.player play];
    }
}

- (void)didSwipe:(UISwipeGestureRecognizer*)swipe{
    if (swipe.direction == UISwipeGestureRecognizerDirectionDown) {
        NSLog(@"Swipe Down");
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - Media Controller Events

- (IBAction)mediaController:(id)sender {
    if (self.player.rate == 1.0) {
        [self.player pause];
    } else {
        [self.player play];
    }
}

- (IBAction)shareRadioStation:(id)sender {
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        mySLComposerSheet = [[SLComposeViewController alloc] init];
        mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [mySLComposerSheet setInitialText:[NSString stringWithFormat:(@"I'm listening the track %@ using Lerigos Music service.", self.stationTrack.text), mySLComposerSheet.serviceType]];
        [mySLComposerSheet addImage:self.stationImage.image];
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSString *output;
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                output = @"You canceled sending your tweets on Twitter. All changes are canceled.";
                break;
            case SLComposeViewControllerResultDone:
                output = @"Your tweet has been successfully published. You can monitor the activity of your post on Twitter, or from your device. Thank you for using Lerigos Music service.";
                break;
            default:
                break;
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter" message:output delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
    }];
}

- (IBAction)likeRadioStation:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Like this station" message:@"This feature you can show in release version!" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    [alert show];
}

@end
