//
//  RegisterAtConnect.m
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 24.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import "RegisterAtConnect.h"
#import <Parse/Parse.h>

@interface RegisterAtConnect ()

@end

@implementation RegisterAtConnect

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [scrollFields setScrollEnabled:true];
    [scrollFields setContentSize:CGSizeMake(375, 667)];
    
    NSLayoutConstraint *leftConstraint =[NSLayoutConstraint
                                         constraintWithItem:scrollFields
                                         attribute:NSLayoutAttributeLeading
                                         relatedBy:0
                                         toItem:self.view
                                         attribute:NSLayoutAttributeLeft
                                         multiplier:1.0
                                         constant:0];
    [self.view addConstraint:leftConstraint];
    
    NSLayoutConstraint *rightConstraint =[NSLayoutConstraint
                                          constraintWithItem:scrollFields
                                          attribute:NSLayoutAttributeTrailing
                                          relatedBy:0
                                          toItem:self.view
                                          attribute:NSLayoutAttributeRight
                                          multiplier:1.0
                                          constant:0];
    [self.view addConstraint:rightConstraint];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.username setDelegate:self];
    [self.password setDelegate:self];
    [self.firstName setDelegate:self];
    [self.lastName setDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Function hide keyboard

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.username resignFirstResponder];
    [self.password resignFirstResponder];
    [self.firstName resignFirstResponder];
    [self.lastName resignFirstResponder];
    
    return YES;
}

- (void)keyboardWasShown:(NSNotification *)notification {
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0);
    scrollFields.contentInset = contentInsets;
    scrollFields.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollFields.contentInset = contentInsets;
    scrollFields.scrollIndicatorInsets = contentInsets;
}

#pragma mark - Operation using the buttons

- (IBAction)registerWithCredentials:(id)sender {
    PFUser *user = [PFUser user];
    user.username = self.username.text;
    user.password = self.password.text;
    user.email = self.username.text;
    
    user[@"firstName"] = self.firstName.text;
    user[@"lastName"] = self.lastName.text;
    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            [self dismissViewControllerAnimated:true completion:nil];
        } else {
            // NSString *errorString = [error userInfo][@"error"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lerigos Music" message:@"Sorry. We can not register your profile because of the error. Please check the information you entered and try again." delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
            [alert show];
        }
    }];
}

- (IBAction)termsInformationCenter:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.apple.com"]];
}

@end
