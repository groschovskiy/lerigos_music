//
//  LerigosConnectCenter.m
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 06.08.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import "LerigosConnectCenter.h"
#import <Parse/Parse.h>

@interface LerigosConnectCenter ()

@end

@implementation LerigosConnectCenter

- (void)viewDidLoad {
    [super viewDidLoad];

    self.profilePicture.layer.cornerRadius = self.profilePicture.frame.size.width / 2;
    self.profilePicture.clipsToBounds = true;
    self.profilePicture.userInteractionEnabled = true;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.password setDelegate:self];
    [self.firstName setDelegate:self];
    [self.lastName setDelegate:self];
    
    [self getCustomerProfileInformation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Cloud Controller Information Center

- (void)getCustomerProfileInformation {
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"_User"];
    [query getObjectInBackgroundWithId:currentUser.objectId block:^(PFObject *userInformation, NSError *error) {
        self.firstName.text = userInformation[@"firstName"];
        self.lastName.text = userInformation[@"lastName"];
        NSLog(@"%@", userInformation);
    }];
}

#pragma mark - Cloud Thread Controller

- (IBAction)showCustomerLounge:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)closeConnectionWithService:(id)sender {
    [PFUser logOutInBackground];
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)saveProfileInformation:(id)sender {
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"_User"];
    [query getObjectInBackgroundWithId:currentUser.objectId
                                 block:^(PFObject *userInformation, NSError *error) {
                                     userInformation[@"firstName"] = self.firstName.text;
                                     userInformation[@"lastName"] = self.lastName.text;
                                     [userInformation saveInBackground];
                                     
                                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Lerigos Music" message:@"Dear user. Your information has been updated successfully. Please check your changes." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                     [alert show];
                                 }];
}

- (IBAction)getProfileInformation:(id)sender {
    [self getCustomerProfileInformation];
}

@end
