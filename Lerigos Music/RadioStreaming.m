//
//  RadioStreaming.m
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import <Parse/Parse.h>
#import "RadioStreaming.h"
#import "StreamingPlayer.h"
#import "LerigosMusicHeart.h"

@interface RadioStreaming ()

@end

@implementation RadioStreaming

- (void)viewDidLoad {
    [super viewDidLoad];
    [scrollView setScrollEnabled:true];
    [scrollView setContentSize:CGSizeMake(770, 145)];
    
    [self getLerigosMusicHeartTracks];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - MusicHeart™ at Lerigos Music

- (void)getLerigosMusicHeartTracks {
    PFQuery *query = [PFQuery queryWithClassName:@"Heart"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *musicLibrary, NSError *error) {
        if (!error) {
            self.lerigosMusicStack = [[NSArray alloc] initWithArray:musicLibrary];
            self.lerigosMusicArtistName = [self.lerigosMusicStack valueForKey:@"artistName"];
            self.lerigosMusicArtistSong = [self.lerigosMusicStack valueForKey:@"artistSong"];
            self.lerigosMusicArtistStream = [self.lerigosMusicStack valueForKey:@"artistStream"];
            [tableView reloadData];
        }
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.lerigosMusicArtistName count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [self.lerigosMusicArtistSong objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = [self.lerigosMusicArtistName objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *artistName = [self.lerigosMusicArtistName objectAtIndex:indexPath.row];
    NSString *songName = [self.lerigosMusicArtistSong objectAtIndex:indexPath.row];
    NSString *trackStream = [self.lerigosMusicArtistStream objectAtIndex:indexPath.row];
    
    StreamingPlayer *streamingViewController = [[StreamingPlayer alloc] initWithNibName:@"StreamingPlayer" bundle:nil];
    streamingViewController.protocolStationName = artistName;
    streamingViewController.protocolStationTrack = songName;
    streamingViewController.protocolStationStream = trackStream;
    [self presentViewController:streamingViewController animated:YES completion:nil];
}

#pragma mark - Radio Station Streaming URLs

- (IBAction)streamCapitalFM:(id)sender {
    NSString *stationRemoteName = @"Capital FM - London";
    NSString *stationRemoteTrack = @"Exclusive Lerigos Music Partner";
    StreamingPlayer *streamingViewController = [[StreamingPlayer alloc] initWithNibName:@"StreamingPlayer" bundle:nil];
    streamingViewController.protocolStationName = stationRemoteName;
    streamingViewController.protocolStationTrack = stationRemoteTrack;
    streamingViewController.protocolStationStream = @"http://media-ice.musicradio.com:80/CapitalUK";
    [self presentViewController:streamingViewController animated:YES completion:nil];
}

- (IBAction)streamRadioRecord:(id)sender {
    NSString *stationRemoteName = @"Radio Record";
    NSString *stationRemoteTrack = @"Exclusive Lerigos Music Partner";
    StreamingPlayer *streamingViewController = [[StreamingPlayer alloc] initWithNibName:@"StreamingPlayer" bundle:nil];
    streamingViewController.protocolStationName = stationRemoteName;
    streamingViewController.protocolStationTrack = stationRemoteTrack;
    streamingViewController.protocolStationStream = @"http://air.radiorecord.ru:8101/rr_320";
    [self presentViewController:streamingViewController animated:YES completion:nil];
}

- (IBAction)streamBBCOne:(id)sender {
    NSString *stationRemoteName = @"BBC Radio 1";
    NSString *stationRemoteTrack = @"London Music Broadcast";
    StreamingPlayer *streamingViewController = [[StreamingPlayer alloc] initWithNibName:@"StreamingPlayer" bundle:nil];
    streamingViewController.protocolStationName = stationRemoteName;
    streamingViewController.protocolStationTrack = stationRemoteTrack;
    streamingViewController.protocolStationStream = @"http://bbcmedia.ic.llnwd.net/stream/bbcmedia_radio1_mf_p";
    [self presentViewController:streamingViewController animated:YES completion:nil];
}

- (IBAction)streamBBCTwo:(id)sender {
    NSString *stationRemoteName = @"BBC Radio 2";
    NSString *stationRemoteTrack = @"Southampton Melody";
    StreamingPlayer *streamingViewController = [[StreamingPlayer alloc] initWithNibName:@"StreamingPlayer" bundle:nil];
    streamingViewController.protocolStationName = stationRemoteName;
    streamingViewController.protocolStationTrack = stationRemoteTrack;
    streamingViewController.protocolStationStream = @"http://bbcmedia.ic.llnwd.net/stream/bbcmedia_radio2_mf_p";
    [self presentViewController:streamingViewController animated:YES completion:nil];
}

- (IBAction)streamRadioHeart:(id)sender {
    NSString *stationRemoteName = @"Heart Radio - London";
    NSString *stationRemoteTrack = @"London Music in Heart";
    StreamingPlayer *streamingViewController = [[StreamingPlayer alloc] initWithNibName:@"StreamingPlayer" bundle:nil];
    streamingViewController.protocolStationName = stationRemoteName;
    streamingViewController.protocolStationTrack = stationRemoteTrack;
    streamingViewController.protocolStationStream = @"http://media-ice.musicradio.com:80/HeartLondonMP3";
    [self presentViewController:streamingViewController animated:YES completion:nil];
}

- (IBAction)streamLerigosMusic:(id)sender {
    NSString *stationRemoteName = @"Lerigos Music";
    NSString *stationRemoteTrack = @"Russian Best Song of Week";
    StreamingPlayer *streamingViewController = [[StreamingPlayer alloc] initWithNibName:@"StreamingPlayer" bundle:nil];
    streamingViewController.protocolStationName = stationRemoteName;
    streamingViewController.protocolStationTrack = stationRemoteTrack;
    streamingViewController.protocolStationStream = @"http://air.radiorecord.ru:8102/rus_320";
    [self presentViewController:streamingViewController animated:YES completion:nil];
}

- (IBAction)streamBeatsOne:(id)sender {
    NSString *stationRemoteName = @"Best Artist of Week";
    NSString *stationRemoteTrack = @"Oliver Heldens";
    StreamingPlayer *streamingViewController = [[StreamingPlayer alloc] initWithNibName:@"StreamingPlayer" bundle:nil];
    streamingViewController.protocolStationName = stationRemoteName;
    streamingViewController.protocolStationTrack = stationRemoteTrack;
    streamingViewController.protocolStationStream = @"http://52.16.86.221/gateway/track_oliver_heldens.mp3";
    [self presentViewController:streamingViewController animated:YES completion:nil];
}

@end
