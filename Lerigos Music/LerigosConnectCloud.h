//
//  LerigosConnectCloud.h
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface LerigosConnectCloud : UIViewController <CLLocationManagerDelegate, UITextFieldDelegate> {
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    IBOutlet UIScrollView *scrollController;
}

@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *livingCountry;
@property (weak, nonatomic) IBOutlet UIImageView *customerPicture;

@end
