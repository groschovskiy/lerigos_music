//
//  ArtistNewsPublisher.m
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import "ArtistNewsPublisher.h"
#import <Parse/Parse.h>

@interface ArtistNewsPublisher ()

@end

@implementation ArtistNewsPublisher

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.artistMessage setDelegate:self];
    [self loadConnectInformation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Navigation Controller Event

- (IBAction)closeViewController:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

#pragma mark - Receive Artist Information

- (void)loadConnectInformation {
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"_Users"];
    [query getObjectInBackgroundWithId:currentUser.objectId block:^(PFObject *artistCard, NSError *error) {
        self.artistName = artistCard[@"artistName"];
        NSLog(@"%@", artistCard);
    }];
}

#pragma mark - Post Publishing Functional

- (IBAction)sendPostToConnect:(id)sender {
    PFObject *artistPost = [PFObject objectWithClassName:@"Connect"];
    artistPost[@"artistName"] = self.artistName;
    artistPost[@"artistMessage"] = self.artistMessage.text;
    // artistPost[@"artistArtwork"] = self.profileImage.image;
    [artistPost saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Lerigos Connect" message:@"Your post published successfully, stay tuned to the main page of the social network." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles: nil];
            [alertView show];
        } else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Lerigos Connect" message:@"During the post publication error occurred. Check the Internet connection, or try again later. Support the artists: +1 (657) 777-3120." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles: nil];
            [alertView show];
        }
    }];
}

#pragma mark - Keyboard Solution Event

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.artistMessage resignFirstResponder];
    return YES;
}

@end
