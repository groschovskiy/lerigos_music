//
//  RadioStreaming.h
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RadioStreaming : UIViewController {
    IBOutlet UITableView *tableView;
    IBOutlet UIScrollView *scrollView;
}

// Special for music streaming service
@property (strong, nonatomic) NSArray *lerigosMusicStack;
@property (strong, nonatomic) NSArray *lerigosMusicArtistName;
@property (strong, nonatomic) NSArray *lerigosMusicArtistSong;
@property (strong, nonatomic) NSArray *lerigosMusicArtistStream;

// Special for radio streaming service
@property (strong, nonatomic) NSArray *stationName;
@property (strong, nonatomic) NSArray *stationAsset;
@property (strong, nonatomic) NSArray *stationStream;

@end