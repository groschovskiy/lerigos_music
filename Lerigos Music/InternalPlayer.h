//
//  InternalPlayer.h
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVAudioPlayer.h>
#import <AVFoundation/AVPlayerItem.h>

@interface InternalPlayer : UIViewController {
    SLComposeViewController *mySLComposerSheet;
}

@property (weak, nonatomic) IBOutlet UILabel *artistName;
@property (weak, nonatomic) IBOutlet UILabel *artistSong;
@property (weak, nonatomic) IBOutlet UIImageView *artistArtwork;

@property (weak, nonatomic) NSString *protocolArtistName;
@property (weak, nonatomic) NSString *protocolArtistSong;
@property (weak, nonatomic) NSString *protocolArtistArtwork;

@end
