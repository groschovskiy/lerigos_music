//
//  LerigosConnectCell.h
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LerigosConnectCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *artistName;
@property (weak, nonatomic) IBOutlet UILabel *artistMessage;
@property (weak, nonatomic) IBOutlet UIImageView *artistArtwork;

@end
