//
//  LerigosConnectLounge.h
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 06.08.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LerigosConnectLounge : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *firstName;
@property (weak, nonatomic) IBOutlet UILabel *lastName;
@property (weak, nonatomic) IBOutlet UILabel *accountType;
@property (weak, nonatomic) IBOutlet UIImageView *profilePicture;

@end
