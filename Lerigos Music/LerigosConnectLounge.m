//
//  LerigosConnectLounge.m
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 06.08.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import "LerigosConnectLounge.h"
#import "LerigosConnectCenter.h"
#import <Parse/Parse.h>

@interface LerigosConnectLounge ()

@end

@implementation LerigosConnectLounge

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.profilePicture.layer.cornerRadius = self.profilePicture.frame.size.width / 2;
    self.profilePicture.clipsToBounds = true;
    self.profilePicture.userInteractionEnabled = true;
    
    [self getCustomerProfileInformation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Cloud Controller Information Center

- (void)getCustomerProfileInformation {
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"_User"];
    [query getObjectInBackgroundWithId:currentUser.objectId block:^(PFObject *userInformation, NSError *error) {
        self.firstName.text = userInformation[@"firstName"];
        self.lastName.text = userInformation[@"lastName"];
        NSLog(@"%@", userInformation);
    }];
}

#pragma mark - Operation using the buttons

- (IBAction)showConnectMessages:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)showConnectCloudCenter:(id)sender {
    LerigosConnectCenter *lerigosCenter = [[LerigosConnectCenter alloc] initWithNibName:@"LerigosConnectCenter" bundle:nil];
    [self presentViewController:lerigosCenter animated:true completion:nil];
}

- (IBAction)followingArtist:(id)sender {
    
}

- (IBAction)restorePurchase:(id)sender {
    UIAlertView *purchaseRestore = [[UIAlertView alloc] initWithTitle:@"Lerigos Music" message:@"Dear user. Your purchases have been successfully restored. Please wait for 1-2 minutes until the changes become effective. Since Apple's server sends information to us immediately." delegate:nil cancelButtonTitle:@"Great!" otherButtonTitles:nil];
    [purchaseRestore show];
}

- (IBAction)lerigosSense:(id)sender {
    UIAlertView *purchaseRestore = [[UIAlertView alloc] initWithTitle:@"Lerigos Music" message:@"Lerigos Music Sense ™ is not available in your area due to restrictions in the regulations. See the list of countries officially support this feature can be on the company's website and read the license agreement." delegate:nil cancelButtonTitle:@"Great!" otherButtonTitles:nil];
    [purchaseRestore show];
}

- (IBAction)supportCalling:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:+16577773120"]];
}

- (IBAction)developmentFunctional:(id)sender {
    UIAlertView *purchaseRestore = [[UIAlertView alloc] initWithTitle:@"Lerigos Music" message:@"The new features you can get immediately after our updates. This feature is enabled by default to full release. Please follow the information in our social networks." delegate:nil cancelButtonTitle:@"Great!" otherButtonTitles:nil];
    [purchaseRestore show];
}

@end
