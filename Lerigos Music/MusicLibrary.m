//
//  MusicLibrary.m
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 23.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import "MusicLibrary.h"
#import "InternalPlayer.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MediaPlayer/MPMediaItem.h>

@interface MusicLibrary ()

@end

@implementation MusicLibrary

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.artistsQuery = [MPMediaQuery songsQuery];
    //Group by Album Artist
    [self.artistsQuery setGroupingType:MPMediaGroupingAlbumArtist];
    //Grab the "MPMediaItemCollection"s and store it in "artistsArray"
    self.artistsArray = [self.artistsQuery collections];
    
    //We then populate an array "artists" with the individual "MPMediaItem"s that self.artistsArray` contains
    NSMutableArray *artists = [NSMutableArray array];
    
    for (MPMediaItemCollection *artist in _artistsArray) {
        //Grab the individual MPMediaItem representing the collection
        MPMediaItem *representativeItem = [artist representativeItem];
        //Store it in the "artists" array
        [artists addObject:representativeItem];
    }
    
    self.sectionedArtistsArray = [self partitionObjects:artists collationStringSelector:@selector(albumArtist)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSArray *)partitionObjects:(NSArray *)array collationStringSelector:(SEL)selector {
    self.collation = [UILocalizedIndexedCollation currentCollation];
    NSInteger sectionCount = [[_collation sectionTitles] count];
    NSMutableArray *unsortedSections = [NSMutableArray arrayWithCapacity:sectionCount];
    for(int i = 0; i < sectionCount; i++)
        [unsortedSections addObject:[NSMutableArray array]];
    
    for (id object in array)
    {
        NSInteger index = [self.collation sectionForObject:object collationStringSelector:selector];
        [[unsortedSections objectAtIndex:index] addObject:object];
    }
    NSMutableArray *sections = [NSMutableArray arrayWithCapacity:sectionCount];
    for (NSMutableArray *section in unsortedSections)
        [sections addObject:[self.collation sortedArrayFromArray:section collationStringSelector:selector]];
    
    return sections;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [[self.collation sectionTitles] objectAtIndex:section];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return [self.collation sectionIndexTitles];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.collation sectionTitles] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.sectionedArtistsArray objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    //We grab the MPMediaItem at the nth indexPath.row corresponding to the current section (or letter/number)
    MPMediaItem *temp = [[self.sectionedArtistsArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    //Title the current cell with the Album artist
    cell.textLabel.text = [temp valueForProperty:MPMediaItemPropertyAlbumArtist];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *songName = [self.artistsArray objectAtIndex:indexPath.row];
    
    InternalPlayer *playerController = [[InternalPlayer alloc] initWithNibName:@"InternalPlayer" bundle:nil];
    playerController.protocolArtistName = [self.artistsArray objectAtIndex:indexPath.row];
    playerController.protocolArtistSong = [self.artistsArray objectAtIndex:indexPath.row];
    [self presentViewController:playerController animated:YES completion:nil];
}

@end
