//
//  ArtistClubLounge.m
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import "ArtistClubLounge.h"
#import "ArtistNewsPublisher.h"

@interface ArtistClubLounge ()

@end

@implementation ArtistClubLounge

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.artistImage.layer.cornerRadius = self.artistImage.frame.size.width / 2;
    self.artistImage.clipsToBounds = true;
    self.artistImage.userInteractionEnabled = true;
    
    [self receiveArtistInformation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Lerigos Connect Artist Lounge Information

- (void)receiveArtistInformation {
    
}

#pragma mark - Navigation Bar Functional

- (IBAction)composeMessageToConnect:(id)sender {
    ArtistNewsPublisher *publisher = [[ArtistNewsPublisher alloc] initWithNibName:@"ArtistNewsPublisher" bundle:nil];
    [self presentViewController:publisher animated:true completion:nil];
}

- (IBAction)closeLoungeConnect:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

@end
