//
//  LerigosConnectTimeline.h
//  Lerigos Music
//
//  Created by Dmitriy Groschovskiy on 21.07.15.
//  Copyright (c) 2015 Groschovskiy Technology PLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LerigosConnectTimeline : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    NSArray *artistName;
    NSArray *artistMessage;
    IBOutlet UITableView *tableView;
}

@property (strong, nonatomic) NSArray *ninjas;

@end
